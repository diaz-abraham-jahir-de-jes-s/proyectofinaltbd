CREATE TABLE Addresses (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Address] VARCHAR(MAX) NOT NULL
);

INSERT INTO Addresses Values(
	'Calle siempre viva 2331, int 73'
)


CREATE TABLE Users (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdAddress INT NULL FOREIGN KEY REFERENCES Addresses (Id),
	[Name] VARCHAR(MAX) NOT NULL,
	EMail VARCHAR(MAX) NOT NULL
);

INSERT INTO Users Values(
	1,
	'Alfredo Robles',
	'Alfredo@gmail.com'
)

CREATE TABLE Prescriptions (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdUser INT NOT NULL FOREIGN KEY REFERENCES Users (Id),
	[Name] VARCHAR(MAX) NOT NULL,
	[Description] VARCHAR(MAX) NOT NULL,
	MedicationList VARCHAR(MAX) NULL,
	StartDate DATETIME NULL,
	EndDate DATETIME NULL
);

INSERT INTO Prescriptions Values(
	'1',
	'Unicomicosis',
	'Tratamiento para pie de atleta',
	'SilkaMedic',
	GETDATE(),
	NULL
)

CREATE TABLE MedicalHistories (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdUser INT NOT NULL FOREIGN KEY REFERENCES Users (Id),
	[Descripcion] VARCHAR(MAX) NOT NULL
);

INSERT INTO MedicalHistories Values(
	1,
	'Medico con historial de problemas dermatologicos en fecha X, Y, Z'
)

CREATE TABLE Specialities (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Name] VARCHAR(MAX) NOT NULL,
	[Description] VARCHAR(MAX) NOT NULL
);

INSERT INTO Specialities Values(
	'Oncologia',
	'Doctor especializado en oncologia y problemas dermatologicos'
)

CREATE TABLE Clinics (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdAddress INT NULL FOREIGN KEY REFERENCES Addresses (Id),
	[Name] VARCHAR(MAX) NOT NULL
);

INSERT INTO Clinics Values(
	1,
	'Clinica No. 20'
)

CREATE TABLE Doctors (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdSpecialty INT NOT NULL FOREIGN KEY REFERENCES Specialities (Id),
	IdClinic INT NOT NULL FOREIGN KEY REFERENCES Clinics (Id),
	[Name] VARCHAR(MAX) NOT NULL,
	Age INT NOT NULL,
	LastName VARCHAR(MAX) NOT NULL
);

INSERT INTO Doctors Values(
	1,
	1,
	'Valentin',
	56,
	'Elizalde'
)

CREATE TABLE Consultations (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdUser INT NOT NULL FOREIGN KEY REFERENCES Users (Id),
	IdDoctor INT NOT NULL FOREIGN KEY REFERENCES Doctors (Id),
	[Description] VARCHAR(MAX) NOT NULL,
	[Date] DATETIME NULL
);

INSERT INTO Consultations Values(
	1,
	1,
	'Dolor de pies',
	GETDATE()
)

CREATE TABLE Patients (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdUser INT NOT NULL FOREIGN KEY REFERENCES Users (Id),
	[Name] VARCHAR(MAX) NOT NULL,
	[Description] VARCHAR(MAX) NOT NULL,
	MedicationList VARCHAR(MAX) NULL,
	StartDate DATETIME NULL,
	EndDate DATETIME NULL
);

INSERT INTO Patients Values(
	'1',
	'Unicomicosis',
	'Tratamiento para pie de atleta',
	'SilkaMedic',
	GETDATE(),
	NULL
)

CREATE TABLE Offices (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Name] VARCHAR(MAX) NOT NULL,
	[Description] VARCHAR(MAX) NOT NULL
);

INSERT INTO Offices Values(
	'Oncologia',
	'Oncologia'
)

CREATE TABLE Appointments (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdUser INT NOT NULL FOREIGN KEY REFERENCES Users (Id),
	IdDoctor INT NOT NULL FOREIGN KEY REFERENCES Doctors (Id),
	IdOffice INT NOT NULL FOREIGN KEY REFERENCES Offices (Id),
	[Date] DATETIME NULL
);

INSERT INTO Appointments Values(
	1,
	1,
	1,
	GETDATE()
)

CREATE TABLE Score (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdAppointment INT NOT NULL FOREIGN KEY REFERENCES Appointments (Id),
	[Description] VARCHAR(MAX) NOT NULL,
	Score INT NOT NULL,
);

INSERT INTO Score Values(
	1,
	'Paciente con pie de atleta',
	10
)

CREATE TABLE Providers (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdAddress INT NULL FOREIGN KEY REFERENCES Addresses (Id),
	[Name] VARCHAR(MAX) NOT NULL,
	PhoneNumber VARCHAR(MAX) NOT NULL,
	[Type] VARCHAR(MAX) NOT NULL,
	Email VARCHAR(MAX) NULL,
	LastName VARCHAR(MAX) NULL
);

INSERT INTO Providers Values(
	1,
	'Similares',
	'6641234567',
	'vendedor',
	'Similares@gmail.com',
	'Similares'
)

CREATE TABLE Products (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdProvider INT NULL FOREIGN KEY REFERENCES Providers (Id),
	[Name] VARCHAR(MAX) NOT NULL,
	Price INT NOT NULL,
	[Date] DATETIME NOT NULL,
	Quantity INT NULL
);

INSERT INTO Products Values(
	1,
	'Silka medic',
	80,
	GETDATE(),
	50
)

CREATE TABLE Waranties (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdProduct INT NULL FOREIGN KEY REFERENCES Products (Id),
	[Date] DATETIME NOT NULL
);

INSERT INTO Waranties Values(
	1,
	GETDATE()
)

CREATE TABLE Inventories (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdProduct INT NULL FOREIGN KEY REFERENCES Products (Id),
	Quantity INT NOT NULL
);

INSERT INTO Inventories Values(
	1,
	50
)

CREATE TABLE Requests (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdProduct INT NULL FOREIGN KEY REFERENCES Products (Id),
	IdUser INT NULL FOREIGN KEY REFERENCES Users (Id),
	Quantity INT NOT NULL,
	StartDate DATETIME NOT NULL,
	EndDate DATETIME NULL
);

INSERT INTO Requests Values(
	1,
	1,
	5,
	GETDATE(),
	NULL
)

CREATE TABLE [Returns] (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdProduct INT NULL FOREIGN KEY REFERENCES Products (Id),
	Reasons VARCHAR(MAX) NOT NULL,
	[Date] DATETIME NOT NULL
);

INSERT INTO [Returns] Values(
	1,
	'Equivocado',
	GETDATE()
)

CREATE TABLE ProductOrders (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdProduct INT NULL FOREIGN KEY REFERENCES Products (Id),
	IdProvider INT NULL FOREIGN KEY REFERENCES Providers (Id),
	Quantity INT NOT NULL,
	StartDate DATETIME NOT NULL,
	EndDate DATETIME NULL
);

INSERT INTO ProductOrders Values(
	1,
	1,
	80,
	GETDATE(),
	NULL
)

CREATE TABLE Materials (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdProvider INT NULL FOREIGN KEY REFERENCES Providers (Id),
	Price INT NOT NULL,
	Brand VARCHAR(MAX) NOT NULL
);

INSERT INTO Materials Values(
	1,
	80,
	'SIlka'
)

CREATE TABLE MaterialOrders (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdMaterial INT NULL FOREIGN KEY REFERENCES Materials (Id),
	IdProvider INT NULL FOREIGN KEY REFERENCES Providers (Id),
	Quantity INT NOT NULL,
	StartDate DATETIME NOT NULL,
	EndDate DATETIME NULL
);

INSERT INTO MaterialOrders Values(
	1,
	1,
	45,
	GETDATE(),
	NULL
)

CREATE TABLE InventoryMaterials (
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	IdMaterial INT NULL FOREIGN KEY REFERENCES Materials (Id),
	Quantity INT NOT NULL
);

INSERT INTO InventoryMaterials Values(
	1,
	45
)

SELECT * FROM Addresses
SELECT * FROM Appointments
SELECT * FROM Clinics
SELECT * FROM Consultations
SELECT * FROM Doctors
SELECT * FROM Inventories
SELECT * FROM InventoryMaterials
SELECT * FROM MaterialOrders
SELECT * FROM Materials
SELECT * FROM MedicalHistories
SELECT * FROM Offices
SELECT * FROM Patients
SELECT * FROM Prescriptions
SELECT * FROM ProductOrders
SELECT * FROM Products
SELECT * FROM Providers
SELECT * FROM Requests
SELECT * FROM [Returns]
SELECT * FROM Score
SELECT * FROM Specialities
SELECT * FROM Users
SELECT * FROM Waranties